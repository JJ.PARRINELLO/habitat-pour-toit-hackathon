-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 11 Janvier 2019 à 09:40
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.2.9-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `habitatpourtoit`
--

-- --------------------------------------------------------

--
-- Structure de la table `abattements`
--

CREATE TABLE `abattements` (
  `id` int(10) UNSIGNED NOT NULL,
  `taux_li` double DEFAULT NULL,
  `taux_cs_cts` double DEFAULT NULL,
  `codes_zones_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `abattements`
--

INSERT INTO `abattements` (`id`, `taux_li`, `taux_cs_cts`, `codes_zones_id`) VALUES
(1, 0.1, 0.15, 3),
(2, 0.1, 0.15, 2),
(3, 0.1, 0.15, 1),
(4, 0.1, 0.15, 4);

-- --------------------------------------------------------

--
-- Structure de la table `codes_zones`
--

CREATE TABLE `codes_zones` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `codes_zones`
--

INSERT INTO `codes_zones` (`id`, `code`) VALUES
(1, 'A'),
(2, 'B1'),
(3, 'B2'),
(4, 'C');

-- --------------------------------------------------------

--
-- Structure de la table `durees_travaux`
--

CREATE TABLE `durees_travaux` (
  `id` int(10) UNSIGNED NOT NULL,
  `duree` int(1) UNSIGNED DEFAULT NULL,
  `avec_travaux` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `loyers_accessoires`
--

CREATE TABLE `loyers_accessoires` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `li` double DEFAULT NULL,
  `l_s_ts` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `loyers_accessoires`
--

INSERT INTO `loyers_accessoires` (`id`, `title`, `li`, `l_s_ts`) VALUES
(1, 'Garage individuel clos', 60, 40),
(2, 'Stationnement en sous sol', 45, 30),
(3, 'Stationnement extérieur privatisé', 30, 20),
(4, 'Jardin (20m² minimum)', 25, 15);

-- --------------------------------------------------------

--
-- Structure de la table `plafonds_locaux`
--

CREATE TABLE `plafonds_locaux` (
  `id` int(11) UNSIGNED NOT NULL,
  `lcs` double(5,2) DEFAULT NULL,
  `lcts` double(5,2) DEFAULT NULL,
  `li` double(5,2) DEFAULT NULL,
  `lo` double(5,2) DEFAULT NULL,
  `codes_zones_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `plafonds_locaux`
--

INSERT INTO `plafonds_locaux` (`id`, `lcs`, `lcts`, `li`, `lo`, `codes_zones_id`) VALUES
(1, 8.00, 7.00, 12.00, 15.00, 3),
(2, 9.00, 8.00, 13.00, 16.00, 2);

-- --------------------------------------------------------

--
-- Structure de la table `plafond_local`
--

CREATE TABLE `plafond_local` (
  `id` int(11) UNSIGNED NOT NULL,
  `lcs` double(5,2) DEFAULT NULL,
  `lcts` double(5,2) DEFAULT NULL,
  `li` double(5,2) DEFAULT NULL,
  `lo` double(5,2) DEFAULT NULL,
  `codes_zones_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `plafond_local`
--

INSERT INTO `plafond_local` (`id`, `lcs`, `lcts`, `li`, `lo`, `codes_zones_id`) VALUES
(1, 8.00, 7.00, 12.00, 15.00, 3),
(2, 9.00, 8.00, 13.00, 16.00, 2);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `short_name` varchar(45) DEFAULT NULL,
  `long_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `short_name`, `long_name`) VALUES
(1, 'ADM', 'administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles_id` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `roles_id`) VALUES
(3, 'Admin', 'Admin', 'admin@habitatpourtoit.org', '$2y$10$5bjkyKbM.z2.IkEdFldFg.jnOhqUqYPoZirt2Nua4RmQhB0AtFh8a', 1);

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

CREATE TABLE `villes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code_postal` int(7) UNSIGNED DEFAULT NULL,
  `codes_zones_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `villes`
--

INSERT INTO `villes` (`id`, `nom`, `code_postal`, `codes_zones_id`) VALUES
(1, 'Montpellier', 34000, 3),
(2, 'Béziers', 34500, 2),
(3, 'lunel', 34400, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `abattements`
--
ALTER TABLE `abattements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_abattements_codes_zones1_idx` (`codes_zones_id`);

--
-- Index pour la table `codes_zones`
--
ALTER TABLE `codes_zones`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `durees_travaux`
--
ALTER TABLE `durees_travaux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `loyers_accessoires`
--
ALTER TABLE `loyers_accessoires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plafonds_locaux`
--
ALTER TABLE `plafonds_locaux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `plafond_local`
--
ALTER TABLE `plafond_local`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_roles_idx` (`roles_id`);

--
-- Index pour la table `villes`
--
ALTER TABLE `villes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_villes_codes_zones1_idx` (`codes_zones_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `abattements`
--
ALTER TABLE `abattements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `codes_zones`
--
ALTER TABLE `codes_zones`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `durees_travaux`
--
ALTER TABLE `durees_travaux`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `loyers_accessoires`
--
ALTER TABLE `loyers_accessoires`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `villes`
--
ALTER TABLE `villes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `abattements`
--
ALTER TABLE `abattements`
  ADD CONSTRAINT `fk_abattements_codes_zones1` FOREIGN KEY (`codes_zones_id`) REFERENCES `codes_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `villes`
--
ALTER TABLE `villes`
  ADD CONSTRAINT `fk_villes_codes_zones1` FOREIGN KEY (`codes_zones_id`) REFERENCES `codes_zones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

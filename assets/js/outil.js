var accessoire = new Array();
  $( function() {
    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }

 
    $( "#ville" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "/get-ville",
          dataType: "json",
          type : "POST",
          data: {
            nomVille : $('#ville').val()
          },
          success: function( data ) {
            response( data );
          
          }
        } );
      },
      minLength: 3,
      select: function( event, ui ) {
        log(  ui.item.value);
      }
    } );

    $('#result_85, #result_autre').hide();

    $('#valider').click(function() {

      var ville = $('#ville').val();
      var surfaceHabitable = $('#surface').val();
      var surfaceAnnexe = $('#surface_annexe').val();
      $("input[type='checkbox']").each(
          function() {

            accessoire += ($(this).attr('id'));

          });
          var donnee = {
            ville,
            surfaceHabitable,
            surfaceAnnexe,
            accessoire , 
          
          } 
         
          $.ajax({
            type: 'POST',
            dataType :'json',
            data: donnee,
            url: "api/cosse",

            success: function(data) {
              console.log(data);
              var calcul85 = data.calcul_85;
              var calculAutre = data.calcul_autre;
			  var abattement85_lcts= calcul85.abattement_lcts.toFixed(2);
			  var abattementAutre_lcts = calculAutre.abattement_lcts.toFixed(2);
			  var abbatement85_li = calcul85.abattement_li.toFixed(2);
			  var abbatementAutre_li = calculAutre.abattement_li.toFixed(2);

              var readCalcul85 = "" +
				  "<div class='display-flex-column'>" +
				  "		<p class='title-result'>Loyer observé</p>" +
				  "		<p class='result'>"+calcul85.lo+"</p>" +
				  "		<p class='title-result'>Abattement</p>" +
				  "		<p class='result'>"+calcul85.abattement_lo+"</p>" +
				  "</div> " +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Intermédiaire</p>" +
				  "		<p>"+calcul85.li+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+abbatement85_li+"</p>" +
				  "</div>" +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Social</p>" +
				  "		<p>"+calcul85.lcs+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+calcul85.abattement_lcs+"</p>" +
				  "</div>" +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Trés Social</p>" +
				  "		<p>"+calcul85.lcts+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+abattement85_lcts+"</p>" +
				  "</div>"
              $("#result_85").empty().append(readCalcul85);
              
              var readCalculAutre = "" +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer observé</p>" +
				  "		<p>"+calculAutre.lo+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+calculAutre.abattement_lo+"</p>" +
				  "</div> " +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Intermédiaire</p>" +
				  "		<p>"+calculAutre.li+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+abbatementAutre_li+"</p>" +
				  "</div>" +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Social</p>" +
				  "		<p>	"+calculAutre.lcs+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+calculAutre.abattement_lcs+"</p>" +
				  "</div>" +
				  "<div class='display-flex-column'>" +
				  "		<p>Loyer Trés Social</p>" +
				  "		<p>"+calculAutre.lcts+"</p>" +
				  "		<p>Abattement</p>" +
				  "		<p>"+abattementAutre_lcts+"</p>" +
				  "</div>"
              $("#result_autre").empty().append(readCalculAutre);
            }
          })

		$('#result_85, #result_autre').show();
    });
  
  } );

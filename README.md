<!--
--	Projet Habitat pour toit dans le cadre de la formation Développeur Logiciel de niveau III
--	Beweb - École Régionale du Numérique session Février 2018 - Janvier 2019
--	
--	@author : Jean-Jacques PARRINELLO
--	@email 	: padbrain@free.fr
--	@Pseudo : padbrain
--	
-->

# Installation

## Par téléchargement de l'archive
Décompresser [l'archive](https://gitlab.com/habitatpourtoit/habitatpourtoit/-/archive/master/habitatpourtoit-master.tar.gz) sur votre serveur.

### En clonant le dépôt gitlab
Saisir la commande suivante à la racine de votre serveur : 

#### En ssh
```
git clone git@gitlab.com:habitatpourtoit/habitatpourtoit.git
```

#### En https
```
git clone https://gitlab.com/habitatpourtoit/habitatpourtoit.git

```

## Création d'un virtual-host

`www.habitatpourtoit.org`

Cette étape est facultative et n'est pas détaillée dans ce fichier.


## Création de la base de données

### Base de données avec quelques fixtures pour tester l'application
Importer, via phpmyadmin, le fichier `habitatpourtoit.sql` situé dans le dossier database situé lui même à la racine du projet.

### Dans les deux cas, l'utilisateur est :
  - Identifiant : admin@habitatpourtoit.org
  - MDP : admin

## Configuration de l'application
Deux fichiers de configuration doivent être édités afin de paramétrer l'url de base de l'application et l'accès à la base de données.

Rendez vous, ensuite, dans le fichier `application/config/config.php` à la ligne 26 et renseigner le paramètre

`config['base_url']` avec le virtual-host précédemment créé ou bien avec la valeur `http://localhost/dossier_du_projet`.

Ouvrir ensuite le fichier `application/config/database.php` et saisir les paramètres d'accès à votre base MySql :

*exemple :*

  - 'hostname' => 'localhost'
  - 'username' => 'username'
  - 'password' => 'password'
  - 'database' => 'nom_de_votre_base'

**L'application est maintenant accessible en saisissant l'url, précédemment définie, dans la barre d'adresse de votre navigateur.**
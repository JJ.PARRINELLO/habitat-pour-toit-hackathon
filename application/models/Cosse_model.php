<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 11/10/18
 * Time: 22:02
 */

class Cosse_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function plafonds_locaux($ville){
		$query = "
				SELECT
				  pl.id pl_id,
				  pl.lcs,
				  pl.lcts,
				  pl.li,
				  pl.lo,
				  cz.id cz_id,
				  cz.code,
				  v.id ville_id,
				  v.nom v_nom,
				  v.code_postal 
				FROM
				  `plafonds_locaux` pl
				INNER JOIN
					`codes_zones` cz
				ON	
					cz.id = pl.codes_zones_id
				INNER JOIN
					`villes` v
				ON
					v.codes_zones_id = cz.id
				WHERE
					v.nom = '$ville'
		";
		//var_dump($query);
		$query = $this->db->query($query);

		return $query->row();
	}

	public function abattements($ville){
		$query = "
				SELECT
				  a.id a_id,
				  a.taux_li,
				  a.taux_cs_cts,
				  cz.id cz_id,
				  cz.code,
				  v.id ville_id,
				  v.nom v_nom,
				  v.code_postal 
				FROM
				  `abattements` a
				INNER JOIN
					`codes_zones` cz
				ON	
					cz.id = a.codes_zones_id
				INNER JOIN
					`villes` v
				ON
					v.codes_zones_id = cz.id
				WHERE
					v.nom =  '$ville'
		";
//		var_dump($query);

		$query = $this->db->query($query, $ville);

		return $query->row();
	}

	public function loyers_accessoires(){
		$query = "
				SELECT
				  *
				FROM
				  `loyers_accessoires`
		";

		$query = $this->db->query($query);

		return $query->result_array();
	}
}

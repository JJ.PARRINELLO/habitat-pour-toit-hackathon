<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="habitatpourtoit ,des données pour un meilleur système de santé. " />
	<meta name="keywords" content="habitatpourtoit,santé,data" />
	<title>Un Habitat Pour Toi </title>
	<meta name="Content-Language" content="fr">
	<meta name="author" content="habitatpourtoit" />
	<meta name="Subject" content="Kanopy,des données pour un meilleur système de santé ">
	<meta name="Copyright" content="habitatpourtoit">
	
	
	<link rel="stylesheet" href="<?= base_url() ?>assets/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="<?= base_url() ?>assets/maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/reset.css">
 	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin.css" media="screen" /> 

	
	<?php 
		if(isset($css_files)):
			foreach($css_files as $file): ?>
				<link rel='stylesheet' href="<?= $file ?>"/>
	<?php 
			endforeach;
		endif;
	?>
 <!-- <link rel="stylesheet" href="<?php //echo base_url() ?>assets/css/style.css" media="screen" />  -->
</head>

<body>
	<div class="habitatpourtoit">
		<header>
			<nav id="main-nav" class="hidden-xs hidden-sm">
				<ul class="menu clearfix">
					<li class="caviar top-menu">
						<a href="http://www.habitatpourtoit.org">Retour au site</a>
					</li>
					<li class="caviar top-menu">
						<a href="<?= base_url() ?>loi-cosse">Comparateur</a>
					</li>
					
					<li class="caviar top-menu">
						<a href="<?= base_url() ?>villes">Gestion des villes</a>
					</li>
					<li class="caviar top-menu">
						<a href="<?= base_url() ?>abattements">Gestion Abattements</a>
					</li>
					<li class="caviar top-menu">
						<a href="<?= base_url() ?>plafond">Plafonds loyers</a>
					</li>
					<li class="caviar top-menu">
						<a href="<?= base_url() ?>logout">Déconnexion</a>
					</li>
				

				</ul>
			</nav>
	</div>

	<div class="contenueCrud">
	<h1 id="outil"><?= $title ?></h1>

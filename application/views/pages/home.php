<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="KanopyMed ,des données pour un meilleur système de santé. " />
    <meta name="keywords" content="kanopyMed,santé,data" />
    <title>Un Habitat Pour Toi </title>
    <meta name="Content-Language" content="fr">
    <meta name="author" content="KanopyMed" />
<!--    <meta name="Subject" content="">-->
    <meta name="Copyright" content="">
    <link rel="stylesheet" href="assets/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="assets/css/style.css" media="screen" />
</head>

<body>
    <div class="KanopyMed">
        <header class="image-in-background">
            <nav id="contact-nav" class="hidden-xs">
                <ul class="clearfix">
                    <li><a href="https://twitter.com/kanopyMed" class="twitter-nav" target="_blank"><span class="fa fa-twitter fa-2x" aria-hidden="true"></span></a>
                    </li>
                    <li><a href="https://fr.linkedin.com/in/kanopyMed/" class="linkedin-nav" target="_blank"><span class="fa fa-linkedin fa-2x" aria-hidden="true"></span></a>
                    </li>
                    <li><a href="kanopyMed@gmail.com" class="email-nav" target="_blank"><span class="fa fa-envelope fa-2x" aria-hidden="true"></span></a>
                    </li>
                    <li class="form-nav"><a href="#KanopyMed-Contact"><span class="fa fa-comment fa-2x" aria-hidden="true"></span></a>
                    </li>
                </ul>
            </nav>
            <nav id="main-nav" class="hidden-xs hidden-sm">
                <ul class="menu clearfix">
                    <li class="caviar top-menu"><a href="#KanopyMed-About">Domaine d'Activité</a>
                    </li>
                    <li class="caviar top-menu"><a href="#KanopyMed-Skills">L'équipe</a>
                    </li>
                    <li class="caviar top-menu"><a href="#KanopyMed-Experiences">Les valeurs</a>
                    </li>
                    <li class="caviar top-menu"><a href="#KanopyMed-Project">Contact</a>
                    </li>
                   <!--  <li class="caviar top-menu"><a href="dataVisualisation">Data Visualisation</a>
                    </li> -->
                </ul>
            </nav>
            <nav id="SmartNav" class="overlay hidden-md hidden-lg"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="overlay-content"> <a href="#Home" class="active">Accueil</a> <a href="#KanopyMed-About">A propos</a> <a href="#KanopyMed-Skills">Compétences</a> <a href="#KanopyMed-Project">KanopyMed</a> <a href="#KanopyMed-Contact">Contact</a> </div>
            </nav> <span class="MenuSmartphone col-xs-1 col-sm-2 hidden-md hidden-lg" onclick="openNav()">&#9776;</span>
            <nav class="hidden-xs">
                <ul class="navigation">
                    <li data-menuanchor="Home" class="active home-nav"><a href="#Home"><span class="fa fa-home fa-2x" aria-hidden="true"></span></a>
                    </li>
                    <li data-menuanchor="KanopyMed-About"><a href="#KanopyMed-About">1</a>
                    </li>
                    <li data-menuanchor="KanopyMed-Skills"><a href="#domaine">2</a>
                    </li>
                    <li data-menuanchor="KanopyMed-Experiences"><a href="#equipe">3</a>
                    </li>
                    <li data-menuanchor="KanopyMed-Project"><a href="#KanopyMed-Experiences">4</a>
                    </li>
                    <li data-menuanchor="KanopyMed-Contact"><a href="#KanopyMed-Project">5</a>
                    </li>
                </ul>
            </nav>
<!--            <div class="container" >
                <div class="row">
                    <div class="crop col-sm-offset-3 col-sm-6 intro-header" > -->
                        <!--<section class="cd-intro">
                    <div class="image-in-background col-sm-12 intro-header" >
                        <img id="photo1" class="img-fluid" src="/assets/images/Photo 210.jpg" alt="logo ou Photo" >
                            <h1 class="cd-headline rotate-1 myName" id="titre"> <span class="cd-words-wrapper"> <b class="is-visible"><em><strong>KANOPYMED</strong></em></b> </span> </h1> </section>
                        <section class="cd-intro">
                            <h1 class="cd-headline push TypoTitle"> <span class="cd-words-wrapper" > <b class="is-visible" ><strong><em>Des données pour un meilleur<br> système de santé</em></strong></b> </span> </h1> </section>
                       
                    </div>
                </div>
            </div> -->
        </header>
        <section id="KanopyMed-About">
            <div class="container-fluid">
                <div class="row">
                    <div class="Space">
                        <div class="container-fluid">
                            <div class="SpaceLeftAbout hidden-xs hidden-sm col-md-8">
                                <p id="descriptifIntro">Une mission d’études et conseils pour l’investissement locatif solidaire  « ancien » ou « neuf » 

Vous êtes une collectivité, un investisseur professionnel, un propriétaire ou un investisseur particulier, une association impliquée dans le logement social ou l'insertion, nous vous accompagnons dans votre projet territorial, votre projet d'investissement, votre projet associatif  pour développer les actions vers louer « solidaire ».

A partir de notre diagnostic, les conditions dans lesquelles chaque investisseur est prêt à s’engager seront respectées par tous les échelons de la chaîne de mise en marché et de gestion.

Une mission de coordination et de mobilisation du parc privé pour la production d’un parc à loyer modéré et accessible aux ménages défavorisés.

Nous assurons la captation et l'apport de parc locatif pour les acteurs de l’Habitat Social.
Nous assurons la coordination du processus captation - contractualisation avec les clients, investisseurs et opérateurs associatifs jusqu’à l’attribution et la mise en location.

Nous proposons la continuité d’accompagnement au-delà et au travers du logement : créer la passerelle pour l’accès aux meilleurs dispositifs d’accompagnement social. </div>
                                
                            <div class="SpaceLight col-md-4">
                                <div class="SpaceRight">
                                    <h3 class="AboutTitle">Pour vous, nous réalisons,</h3>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-4 AboutLeft"> <img class="img-fluid" src="/assets/images/08092015-RC0_1924 copie.jpg" alt="logoMedaillon" id ='logoMedaillon'></div>
                    <div class="col-xs-12 col-md-8 AboutRight">
                    <h1 class="AboutTitle" id="domaine">Notre ambition</h>
                    <p id="ambition"></p>
                    Etre un interlocuteur de référence pour la captation de logements et la coordination des dispositifs.

                    Lever les freins à l’investissement « solidaire » par notre offre de sécurisation et de coordination.

                    Proposer et favoriser l'insertion socio-professionnelle des bénéficiaires dès leurs démarches d'obtention d'un logement.
                    </p>

                        <p class="AboutText" id="presentation2">
							<img src="/assets/images/activity.png" alt="activity" id= "activity">
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section id="KanopyMed-Skills">
            <div class="container-fluid">
                <div class="row">
                    <div class="Space">
                        <div class="container-fluid">
                            <div class="SpaceLeftSkills hidden-xs col-sm-12 col-md-8">
                            
                                 
                                <div class="col-sm-6" id="xavier">
                                    <h3><em>Xavier Péquin</em></h3>
                                    <p>Formateur depuis vingt ans, je me suis aussi investi dans la responsabilité administrative et financière d'entreprises associatives pendant dix années puis ai pris la direction d'un organisme de formation professionnelle pendant quatre années.</p>
                                </div>
                                <div class="col-sm-6" id="sarra"> 
                                    <h2><em>Sarra Gendre</em></h2>
                                    <p>Après une formation initiale et une expérience de cinq années dans l'immobilier privé, je m'investit depuis vingt ans pour développer l'habitat social dans le parc privé de logements au sein d'Adages puis de l'Agence Immobilière à Vocation Sociale Hérault que je dirige depuis sa création en 2007.</p>
                                </div>
                            </div>
                            <div class="SpaceLight hidden-sm col-md-4">
                                <div class="SpaceRight" id="equipe">
                                    <h3 class=" AboutTitle">L'équipe</h3> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                </div>
            </div>
        </section>
        <section class="timeline" id="KanopyMed-Experiences">
            <div class="row">
                <div class="container-fluid">
                    <div class="Space">
                        
                        
                    </div>
                </div>
            </div>
            <div class="row">
                
            </div>
        </section>
        <section id="KanopyMed-Project">
            
            <div class="row">
                <div class="container-fluid">
                    <div class="ProjectPart col-xs-12">
                        <div class="ProjectContent col-xs-12 col-sm-4">

                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times" aria-hidden="true"></span>
                                            </button>
                                            <h4 class="modal-title">Dr Grégoire MERCIER<br>Chief Scientific Officer</h4>
                                            <img class="img-fluid" src="/assets/images/MercierGregoire.png" alt="UlysseRodts" id="UlysseRodts" data-toggle="modal" data-target="#myModal1">
                                            <p id="infoMercier">

                                                Téléphone : +33628356717
                                                <br> Mail : mercier@Kanopymed.com
                                                <br> Formation : M.D. (public health)
                                                <br> PhD (health economics and management)
                                                <br> Linkedin :<a id="linkedin" href="https://fr.linkedin.com/in/yassine-reziki/" class="linkedin-nav " target="_blank"><span class="fa fa-linkedin fa-2x" aria-hidden="true"></span></a>
                                                <br> Twiter :<a href="https://twitter.com/yreziki" class="twitter-nav" target="_blank" id="twiterMercier"><span class="fa fa-twitter fa-2x" aria-hidden="true"></span></a>
                                            </p>

                                            </p>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div id="myModalImg2" class="modal-img"> <span class="close-img2">×</span> <img class="modal-content-img" id="img02">
                                                            <div id="caption2"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">

                                                        <p id="detailMercier">Grégoire est médecin de santé publique, titulaire d’un master en économie de la santé et d’une thèse en gestion. Après avoir travaillé somme épidémiologiste, il a créé l’Unité de Recherche Médico-économique du CHRU de Montpellier. Chercheur associé au CEPEL (UMR 5112 CNRSUniversité de Montpellier), il a conduit de nombreux travaux sur les parcours de soins à partir des données de santé (PMSI, SNIIRAM, SNDS). EN 2018, Grégoire a été sélectionné comme Harkness Fellow in Healthcare Policy and Practice à la Harvard Medical School.</p>
                                                        <br/>
                                                        KanopyMed-partenaire
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section class="timeline" id="KanopyMed-partenaire">
                        <div class="container">
                            <div class="display-flex-row" id="logo-partenaires">
                                <div class="footer">
                                    <p>Adresse :</p>
                                    <p>Tel : </p>
                                    <p><a href="signin"> Webmaster</a></p>
                                </div>
                      <!--           <div class="col-md-6"><img class=img-responsive src="/assets/images/europe.jpg" id="europe"></div>
                                <div class="col-md-6"><img class=img-responsive src="/assets/images/chu.png" id="chu"></div> -->
                                <div class="col-md-6"><img class=img-responsive src="/assets/images/beweb.png" id="beweb"></div>
                                <div class="col-sm-6"><img class=img-responsive src="/assets/images/alter.png" id="alter"></div>
                            </div>
                            
                        </section>
                            <div class="modal fade" id="myModal2" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times" aria-hidden="true"></span>
                                            </button>
                                            <h4 class="modal-title">Ulysse RODTS<br>Chief Executive Officer</h4>
                                            <img class="img-fluid" src="/assets/images/UlysseRODTS.jpg" alt="UlysseRodts" id="UlysseRodts" data-toggle="modal" data-target="#myModal2">
                                            <div class="modal-body">
                                                <p>Téléphone : +33629325346
                                                    <br> Mail : ulysse.rodts@Kanopymed.com
                                                    <br> Formation: M.Sc. (Economics)
                                                    <br> Publications : Point de repère sur l’activité
                                                    <br>Libérale des professionnels de santé <a href="https://www.ameli.fr/fileadmin/user_upload/documents/Points_de_repere_n_50_-_L_activite_des_medecins_liberaux_a_travers_la_CCAM.pdf" id="publicationUlysse" target="_blank">(Ici)</a>
                                                    <br> Linkedin :<a href="https://fr.linkedin.com/in/yassine-reziki/" class="linkedin-nav " id="linkedinUlysse" target="_blank"><span class="fa fa-linkedin fa-2x" aria-hidden="true"></span></a>
                                                    <br> Twiter :<a href="https://twitter.com/yreziki" class="twitter-nav" target="_blank" id="twiterUlysse"><span class="fa fa-twitter fa-2x" aria-hidden="true"></span></a>
                                                </p>

                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                        </div>
                                                        <div class="col-md-8">
                                                            <p id="detailUlysse">
                                                                Ulysse a étudié l’économie et la gestion avant de s’orienter vers l’économie publique. Il a acquis une première expérience dans la santé au sein de l’Unité de Recherche Médico-économique du CHRU de Montpellier, en participant à différents travaux de recherche avec Grégoire. Il a travaillé ensuite pendant un an au Département des Actes médicaux au sein de la Caisse Nationale d’Assurance Maladie en tant que Statisticien. Ses missions étaient principalement de chiffrer des impacts budgétaires lors de négociations tarifaires avec les professionnels de santé via le SNIIRAM (DCIR et PMSI) ainsi que suivre et mesurer l’évolution des dépenses de la CCAM. Après son passage à l’assurance maladie, Ulysse a occupé le poste de data scientist pour un cabinet de conseil dans le secteur de la santé, ce qui l’a amené à réaliser des études de parcours de soins (depuis le SNIIRAM), des mesures d’impact budgétaire ou encore des outils d’aide à la décision, toujours en collaboration avec des laboratoires pharmaceutiques, des assurances ou des institutions publiques.

                                                                <br/>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
        </section>

        </div>
        <script>
            function openNav() {
                document.getElementById("SmartNav").style.width = "80%";
            }

            function closeNav() {
                document.getElementById("SmartNav").style.width = "0%";
            }
        </script>
        <script>
            var modal1 = document.getElementById('myModalImg');
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var modal2 = document.getElementById('myModalImg2');
            var img2 = document.getElementById('myImg2');
            var modalImg2 = document.getElementById("img02");
            var captionText = document.getElementById("caption");
            var captionText2 = document.getElementById("caption2");
/* 
            img.onclick = function() {
                modal1.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            img2.onclick = function() {
                modal2.style.display = "block";
                modalImg2.src = this.src;
                captionText2.innerHTML = this.alt;
            } */
           /*  var span = document.getElementsByClassName("close-img")[0];
            span.onclick = function() {
                modal1.style.display = "none";
            }

            var span = document.getElementsByClassName("close-img2")[0];
            span.onclick = function() {
                modal2.style.display = "none";
            } */
        </script>
        <script src="assets/ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="assets/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/use.fontawesome.com/130f092642.js"></script>
        <script src="assets/js/Animations.min.js" type="text/javascript"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="assets/js/jquery.js" charset="utf-8"></script>
        
</body>

</html>

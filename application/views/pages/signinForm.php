
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="KanopyMed ,des données pour un meilleur système de santé. " />
    <meta name="keywords" content="kanopyMed,santé,data" />
    <title>Un Habitat Pour Toi </title>
    <meta name="Content-Language" content="fr">
    <meta name="author" content="KanopyMed" />
    <meta name="Subject" content="Kanopy,des données pour un meilleur système de santé ">
    <meta name="Copyright" content="KanopyMed">
	<link rel="stylesheet" href="assets/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/reset.css">
	<link rel="stylesheet" href="assets/css/style.css" media="screen" />
</head>

<body>
    <div class="KanopyMed">
        <header>
          
            <nav id="main-nav" class="hidden-xs hidden-sm">
                <ul class="menu clearfix">
                    <li class="caviar top-menu"><a href="http://www.habitatpourtoit.org">Retour au site</a>
                    </li>
                   
                 
                </ul>
            </nav>

<div class="row justify-content-md-center">
    <div class="col-lg-6 col-12" id="signinBlock">
        <h2 class="text-center" id="signin">Connexion</h2>
		<?php echo (($this->session->flashdata('success')) != null) ? $this->session->flashdata('success') : ""; ?>

			<!-- <h1><= $title ?></h1> -->

			<form action = "signin" method="post">
				<?= (($this->session->flashdata('login_error')) != null) ? $this->session->flashdata('login_error') : ""; ?>
				<?= form_error('email'); ?>
				<div class="form-group row">
					<label for="email" class="col-sm-2 col-form-label" >Email : </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="email" value="<?= set_value('email') ?>" id="email"  placeholder="Saisissez votre email">
					</div>
				</div>
				<?= form_error('password'); ?>
				<div class="form-group row">
					<label for="password" class="col-sm-2 col-form-label" >Mot de passe : </label>
					<div class="col-sm-10">
					<input type="password" class="form-control" name="password" value="<?= set_value('password') ?>" id="password"  placeholder="Saisissez votre mot de passe">
					</div>
				</div>

				<div class="form-group row">
					<div class="col-sm-10">
					<input type="submit" name="login" class="btn btn-info" id="login" value="Se connecter">
					</div>				
				</div>

			</form>
	</div>
</div>	
        <script src="assets/ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="assets/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/use.fontawesome.com/130f092642.js"></script>
        <script src="assets/js/Animations.min.js" type="text/javascript"></script>
        <script src="assets/js/modernizr.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="assets/js/jquery.js" charset="utf-8"></script>
        
</body>

</html>

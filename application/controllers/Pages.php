<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 10/09/18
 * Time: 22:31
 */

class Pages extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('html', 'form');
//			$this->load->model('members_model');
	}

	public function view($page='home')
	{

		if (! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			/*   affiche une page d'erreur si la vue n'existe pas */
			show_404();
		}else{
			$data['title'] = ucfirst($page);
			$this->load->view('pages/'.$page, $data);
		}
 	}


	public function signIn(){

//		var_dump($this->securityByTkn);
		//	Title page
 		$datas['title'] = "Identification";

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<p class="alert-danger">', '</p>');
		if ($this->form_validation->run('members/signIn') == FALSE)
		{
//			echo password_hash('sarra', PASSWORD_DEFAULT);
//			die();
			/*
			 *	Errors were found in form user entries
			 * 	Reloading form
			 */
			$this->load->view('pages/signinForm', $datas);

		}
		else
		{
			/*
			 * 	No Error found
			 * 	Let's check for an existing user
			 */
//			$this->load->model('members_model');

			/*
			 * Initialize session with surfer profile
			 */
			$oMember = $this->members_model->retrieveMemberProfile(array($this->input->post()['email']));
var_dump($oMember);
//die();
			if(!is_null($oMember) && password_verify($this->input->post()['password'], $oMember->password) === true){
				$this->_initSession($oMember);
			}else{
				$this->session->set_flashdata('login_error', "<p class='error'>Vos identifiants de connexion ne sont pas correct !</p>");
				redirect('signin');
			}
//			$_SESSION['oMember'] = $this->members_model->retrieveMemberProfile(array($this->input->post()['pseudo'], $this->input->post()['password']));

			/*
			 * Generating token cookie
			 */
			$this->securityByTkn->generateToken($_SESSION['oMember']);
//		die (base_url() . 'loi-cosse');
			redirect('loi-cosse');
		}
	}

}

<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 10/09/18
 * Time: 22:31
 */

class Members extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('members_model');
	}

	public function outil(){
	    $this->load->model('api_model', 'api');

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_error_delimiters('<span class="alert-danger">', '</span>');
		if ($this->form_validation->run('comparateur') == FALSE)
		{
			$datas['accessoires'] = $this->api->getAccessoire();
			$datas['title'] = "Comparateur loi Cosse";
			$this->load->view('members/adm/header', $datas);
			$this->load->view('members/adm/loi-cosse', $datas);
		}
		else
		{

			//	Chargement du modèle Dispositif Cosse
			$this->load->model('cosse_model', 'cosse');

			//	Commune de référence
			$critere['nom'] = $this->input->post('ville');
			$critere['shf'] =  $this->input->post('surface');
			$critere['sa'] =  $this->input->post('surface_annexe');

			//	Récupération des plafonds locaux liés à la commune
			$oPlafonds = $this->cosse->plafonds_locaux($critere['nom']);

			//	Récupération des abattements liés à la commune
			$oAbattements = $this->cosse->abattements($critere['nom']);

			//	Récupération des loyers accessoires
			$aoLoyersAccessoires = $this->cosse->loyers_accessoires();

//			var_dump($oPlafonds);
//			var_dump($oAbattements);
//			var_dump($aoLoyersAccessoires);

			$resultArray = array();

			//	Cas 1 : abattement de 85%
			$critere['abattement'] =  0.85;

			//	1.1 : plafond observé
			$critere['plafond'] = $oPlafonds->lo;
			$resultArray['loyerObserve'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond'], $critere['abattement']);

			// 	1.2 : plafond intermédiaire
			$critere['plafond'] = $oPlafonds->li;
			$resultArray['loyerIntermediaire'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond'], $critere['abattement']);

			//	1.3 : plafond social
			$critere['plafond'] = $oPlafonds->lcs;
			$resultArray['loyerSocial'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond'], $critere['abattement']);

			//	1.4 : plafond très social
			$critere['plafond'] = $oPlafonds->lcts;
			$resultArray['loyerTresSocial'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond'], $critere['abattement']);

			var_dump($resultArray);
		}
	}

	private function calcul_plafond($shf, $sa, $plafond, $abattement){

		//	Calcul de la surface
		$sac = (($sa/2) > 8) ? 8 : $sa/2;
		$S = $shf + $sac;

		// Calcul du coefficient multiplicateur
		$cm = 0.7 + ((19/$S) > 1.2) ? 1.2 : 19/$S;

		// Formule = S * Plafond * cm * abattement
		$resultat = $S * $plafond * $abattement;

		return $resultat;
	}

	public function logout(){

		/*
         * Deactivate token cookie
		 * Unset oMember session
		 * redirect to homepage
         */
		$this->securityByTkn->deactivate();
		$this->session->unset_userdata('oMember');
		redirect('/', 'refresh');
	}
}

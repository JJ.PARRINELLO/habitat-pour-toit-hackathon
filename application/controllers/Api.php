<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 10/09/18
 * Time: 22:31
 */

class Api extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model("api_model", 'api');
    }

    public function ville(){
        $data = $this->input->post()['nomVille'];
        $aoVille = $this->api->getVille($data);
        $aVilles = array();
        foreach($aoVille as $oVille){
            $aVilles[] = $oVille->nom;
        } 
        //var_dump($aVilles);
        echo json_encode($aVilles);
       
    }

    public function cosse(){

		//	Commune de référence
		$critere['nom'] = $this->input->post()['ville'];
		$critere['shf'] = $this->input->post()['surfaceHabitable'];
		$critere['sa'] = $this->input->post()['surfaceAnnexe'];

		//	Chargement du modèle Dispositif Cosse
		$this->load->model('cosse_model', 'cosse');

		//	Récupération des plafonds locaux liés à la commune
		$oPlafonds = $this->cosse->plafonds_locaux($critere['nom']);

		//	Récupération des abattements liés à la commune
		$oAbattements = $this->cosse->abattements($critere['nom']);

		$resultArray = array('calcul_85' => array(), 'calcul_autre' => array());

		//	Cas 1 : abattement de 85%
		$critere['abattement'] =  0.85;

		//	1.1 : plafond observé
		$critere['plafond'] = $oPlafonds->lo;
		$resultArray['calcul_85']['lo'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_85']['abattement_lo'] = "aucun";

		// 	1.2 : plafond intermédiaire
		$critere['plafond'] = $oPlafonds->li;
		$resultArray['calcul_85']['li'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_85']['abattement_li'] = $resultArray['calcul_85']['li'] * $critere['abattement'];

		//	1.3 : plafond social
		$critere['plafond'] = $oPlafonds->lcs;
		$resultArray['calcul_85']['lcs'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_85']['abattement_lcs'] = $resultArray['calcul_85']['lcs'] * $critere['abattement'];

		//	1.4 : plafond très social
		$critere['plafond'] = $oPlafonds->lcts;
		$resultArray['calcul_85']['lcts'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_85']['abattement_lcts'] = $resultArray['calcul_85']['lcts'] * $critere['abattement'];



		//	Cas 2 : abattement autre

		//	1.1 : plafond observé
		$critere['plafond'] = $oPlafonds->lo;
		$resultArray['calcul_autre']['lo'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_autre']['abattement_lo'] = "Aucun";

		// 	1.2 : plafond intermédiaire
		$critere['plafond'] = $oPlafonds->li;
		$critere['abattement'] =  $oAbattements->taux_li;
		$resultArray['calcul_autre']['li'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_autre']['abattement_li'] = $resultArray['calcul_autre']['lo'] * $critere['abattement'];

		//	1.3 : plafond social
		$critere['plafond'] = $oPlafonds->lcs;
		$critere['abattement'] =  $oAbattements->taux_cs_cts;
		$resultArray['calcul_autre']['lcs'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_autre']['abattement_lcs'] = $resultArray['calcul_autre']['lo'] * $critere['abattement'];

		//	1.4 : plafond très social
		$critere['plafond'] = $oPlafonds->lcts;
		$critere['abattement'] =  $oAbattements->taux_cs_cts;
		$resultArray['calcul_autre']['lcts'] = $this->calcul_plafond($critere['shf'], $critere['sa'], $critere['plafond']);
		$resultArray['calcul_autre']['abattement_lcts'] = $resultArray['calcul_autre']['lo'] * $critere['abattement'];

		echo json_encode($resultArray);
	}

	private function calcul_plafond($shf, $sa, $plafond){

		//	Calcul de la surface
		$sac = (($sa/2) > 8) ? 8 : $sa/2;
		$S = $shf + $sac;

		// Calcul du coefficient multiplicateur
		$inter = ((19/$S) > 1.2) ? 1.2 : 19/$S;
		$cm = 0.7 + $inter;

		// Formule = S * Plafond * cm * abattement
		$resultat = $S * $plafond * $cm;

		return $resultat;
	}
    
}

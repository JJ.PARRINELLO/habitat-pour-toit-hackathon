<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grocery_admin extends MY_Controller {

    public function __construct()
	{
        parent::__construct();
        
        $this->load->database();
        
        $this->load->library('grocery_CRUD');
        $this->config->load('grocery_crud');
    }
 
    public function villes(){

        $datas['title'] = 'Gestion des villes';

		$crud = new grocery_CRUD();
		$crud->set_subject('Villes');

        $crud->set_table('villes');
        $crud->set_relation('codes_zones_id','codes_zones','code');

        $crud->columns('nom','code_postal','codes_zones_id');
        $crud->display_as('nom','Nom ville')
				->display_as('code_postal','Code postal')
				->display_as('codes_zones_id','Code zone');

		$crud->fields('nom','code_postal','codes_zones_id'); 
		
		/* $crud->field_type('logo_location', 'text');
		$crud->unset_texteditor('logo_location','full_text'); */
       
        $crud->unset_clone(); 
		$output = $crud->render();
		//var_dump($output);
		$datas['css_files'] = $output->css_files;

        $this->load->view('members/adm/header', $datas);
        $this->load->view('members/crud/crud_view',$output);
    }
 
    public function abattements(){

        $datas['title'] = 'Gestions des Abattements';

		$crud = new grocery_CRUD();
        $crud->set_subject('Abattements');
        $crud->set_table('abattements');
        $crud->set_relation('codes_zones_id','codes_zones','code');

        $crud->columns('taux_li', 'taux_cs_cts','codes_zones_id');
        $crud->fields('taux_li','taux_cs_cts','codes_zones_id');
		$crud->display_as('taux_li','Taux li');
		$crud->display_as('taux_cs_cts','Taux social, très social');
		$crud->display_as('codes_zones_id','Code zone');

		$output = $crud->render();
		$crud->unset_clone(); 
		$datas['css_files'] = $output->css_files;
		
        $this->load->view('members/adm/header', $datas);
        $this->load->view('members/crud/crud_abattements',$output);
	}

	public function plafond(){

        $datas['title'] = 'Gestion des plafonds';

		$crud = new grocery_CRUD();
        $crud->set_subject('Plafonds');
        $crud->set_table('plafond_local');
        $crud->set_relation('codes_zones_id','codes_zones','code');

        $crud->columns('lcs', 'lcts','li', 'lo', 'codes_zones_id');
        $crud->fields('li','lo','lcs','lcts','codes_zones_id');
		$crud->display_as('lo','Loyer observé');
		$crud->display_as('li','Loyer intermédiaire');
		$crud->display_as('lcs','Loyer social');
		$crud->display_as('lcts','Loyer trés social');
		$crud->display_as('codes_zones_id','Code zone');

		$output = $crud->render();
		$crud->unset_clone(); 
		$datas['css_files'] = $output->css_files;
		
        $this->load->view('members/adm/header', $datas);
        $this->load->view('members/crud/crud_plafond',$output);
    }

}

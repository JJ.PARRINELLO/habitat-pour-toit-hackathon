<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 12/09/18
 * Time: 11:13
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['views_rights'] = array(

	/*
	 * routes for 2 members at least
	 */
	''									=> array('ALL' => true),
	'home'								=> array('ALL' => true),
	'signin' 							=> array('ALL' => true),
	'logout' 							=> array('ADM' => true),

	/*
	 * routes for admin
	 */
	'loi-cosse'							=> array('ADM' => true),
	'api/cosse'							=> array('ADM' => true),

	/*
	 * routes for grocery ville
	 */

	'villes'								=> array('ADM' => true),
	'villes/(:any)'							=> array('ADM' => true),
	'villes/(:any)/(:num)'					=> array('ADM' => true),

	/*
	 * routes for grocery abattements
	 */

	'abattements'								=> array('ADM' => true),
	'abattements/(:any)'						=> array('ADM' => true),
	'abattements/(:any)/(:num)'					=> array('ADM' => true),

	/*
	 * routes for grocery plafond
	 */

	'plafond'								=> array('ADM' => true),
	'plafond/(:any)'						=> array('ADM' => true),
	'plafond/(:any)/(:num)'					=> array('ADM' => true),
);

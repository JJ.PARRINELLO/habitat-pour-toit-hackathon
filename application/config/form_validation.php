<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 11/09/18
 * Time: 11:18
 */
	
$config = array(
	'members/signIn' => array(
								array(
									'field' => 'email',
									'label' => 'Email',
									'rules' => 'required|valid_email'
								),
								array(
									'field' => 'password',
									'label' => 'Mot de passe',
									'rules' => 'required|min_length[4]|max_length[12]'
								)
							),

	'comparateur' => array(
								array(
									'field' => 'ville',
									'label' => 'Ville',
									'rules' => 'required'
								),
								array(
									'field' => 'surface',
									'label' => 'Surface',
									'rules' => 'required|numeric'
								),
								array(
									'field' => 'surface_annexe',
									'label' => 'Surface annexe',
									'rules' => 'required|numeric'
								),
							),

	'pages/contact' => array(
								array(
									'field' => 'lastname',
									'label' => 'Nom',
									'rules' => 'required',
								),
								array(
									'field' => 'firstname',
									'label' => 'Prénom',
									'rules' => 'required',
									
								),
								array(
									'field' => 'email',
									'label' => 'Email',
									'rules'=> 'required|valid_email',
									
								),
								array(
									'field' => 'message',
									'label' => 'Message',
									'rules' => 'required'
								)
							), 
);
